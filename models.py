from django.db import models
from geriatrics.models import Admission
from medication.models import Medication

# Create your models here.
class Kardex(models.Model):
    resident = models.OneToOneField(Admission, on_delete=models.CASCADE) 
    known_allergies = models.CharField(max_length=250, blank=True)
    vital_signs_monitoring = models.CharField(max_length=250, blank=True) 
    # Precaution
    fall = models.BooleanField() 
    pressure_ulcer = models.BooleanField() 
    bleeding = models.BooleanField() 
    aspiration = models.BooleanField() 
    infection = models.BooleanField() 
    wandering = models.BooleanField() 
    others = models.BooleanField() 
    specify = models.CharField(max_length=250, blank=True) 

    def __str__(self):
        return f'{self.resident}'

    class Meta:
        verbose_name_plural = 'Kardexes'
        
class OrganSystem(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, unique=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name
        
class Diagnosis(models.Model):
    kardex = models.ForeignKey(Kardex, on_delete=models.CASCADE, null=True)
    system = models.ForeignKey(OrganSystem, on_delete=models.CASCADE, null=True, blank=True)
    date = models.DateField(auto_now=False, null=True, blank=True)
    diagnosis = models.CharField(max_length=250, db_index=True)

    def __str__(self):
        return f'{self.name}' 

    class Meta:
        ordering = ['system']
        verbose_name_plural = 'Diagnoses'
        
    def __str__(self):
        return f'{self.kardex}: {self.date}-{self.diagnosis}'
       
class PrescribedMedication(models.Model):
    diagnosis = models.ForeignKey(Diagnosis, on_delete=models.CASCADE)
    medication = models.ForeignKey(Medication, on_delete=models.CASCADE)	

    def __str__(self):
        return f'{self.diagnosis}/{self.medication}'

class Therapy(models.Model):
    resident = models.ForeignKey(Admission, on_delete=models.CASCADE, limit_choices_to={'discharged': False})
    therapy = models.CharField(max_length=120) 
    schedule = models.TextField()

    def __str__(self):
        return f'{self.resident}: {self.therapy}'

    class Meta:
        verbose_name_plural = 'Therapies'

